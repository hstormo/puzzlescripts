title A Roll of the Dice
author ddarknut
homepage https://ddarknut.itch.io/a-roll-of-the-dice
noaction
again_interval 0.12
background_color black

========
OBJECTS
========

Background 
#118337 #017327  
00000
01111
01111
01111
01111

WallI
#681824

WallS
#681824 #782834
00000
00000
00000
00000
11111

Die1
#eee #222 #ccc
20002
00000
00100
00000
20002

Die2
#eee #222 #ccc
20002
00010
00000
01000
20002

Die3
#eee #222 #ccc
20002
00010
00100
01000
20002

Die4
#eee #222 #ccc
20002
01010
00000
01010
20002

Die5
#eee #222 #ccc
20002
01010
00100
01010
20002

Die6
#eee #222 #ccc
20002
01010
01010
01010
20002

Die7
black

Slot1
#eee #444 #333 #222 #111 #000 
11111
12221
23032
34443
45554

Slot2
#eee #444 #333 #222 #111 #000
11111
12201
23332
30443
45554

Slot3
#eee #444 #333 #222 #111 #000
11111
12201
23032
30443
45554

Slot4
#eee #444 #333 #222 #111 #000
11111
10201
23332
30403
45554

Slot5
#eee #444 #333 #222 #111 #000
11111
10201
23032
30403
45554

Slot6
#eee #444 #333 #222 #111 #000
11111
10201
20302
30403
45554

SlotClosing1
#eee #444 #333 #222 #111 #000 
11111
12221
23332
34443
45554

SlotClosing2
#eee #444 #333 #222 #111 #000 #118337 #017327  
66666
62227
63337
64447
67777

SlotClosing3
#eee #444 #333 #222 #111 #000 #118337 #017327  
66666
67777
67377
67777
67777

PlayerR
orange lightgreen white black
.11..
.000.
02323
00000
.0.0.

PlayerL
orange lightgreen white black
..11.
.000.
32320
00000
.0.0.

RollDown
transparent

RollUp
transparent

RollLeft
transparent

RollRight
transparent

Shift
transparent


=======
LEGEND
=======

Die = Die1 or Die2 or Die3 or Die4 or Die5 or Die6 or Die7
SlotOpen = Slot1 or Slot2 or Slot3 or Slot4 or Slot5 or Slot6
SlotClosing = SlotClosing1 or SlotClosing2 or SlotClosing3
Wall = WallI or WallS
BlocksDice = Wall or Die
Roll = RollLeft or RollUp or RollDown or RollRight
Player = PlayerR or PlayerL 

. = Background
# = WallI
$ = WallS
p = PlayerR
q = PlayerL
1 = Die1
2 = Die2
3 = Die3
4 = Die4
5 = Die5
6 = Die6
a = Slot1
b = Slot2
c = Slot3
d = Slot4
e = Slot5
f = Slot6

=======
SOUNDS
=======

SlotClosing1 create 15057908
Die move 76390907 (63633307)

sfx0 3276502 (79773108) (die falls with wrong value)

================
COLLISIONLAYERS
================

Background
SlotOpen, SlotClosing1, SlotClosing2, SlotClosing3
Player, Wall, Die
Roll, Shift

======
RULES     
======

(animate holes closing)
[ SlotClosing3 ] -> [ ] 
[ SlotClosing2 ] -> [ SlotClosing3 ] again
[ SlotClosing1 ] -> [ SlotClosing2 ] again

(rolling dice 1/2)
up [ RollUp | Die | no BlocksDice ] -> [ | > Die | ] sfx1
left [ RollLeft | Die | no BlocksDice ] -> [ | > Die | ] sfx1
right [ RollRight | Die | no BlocksDice ] -> [ | > Die | ] sfx1
down [ RollDown | Die | no BlocksDice ] -> [ | > Die | ] sfx1
[ RollUp ] -> [ ]
[ RollLeft ] -> [ ]
[ RollRight ] -> [ ]
[ RollDown ] -> [ ]

(player move)
[ left PlayerR ] -> [ left PlayerL ]
[ right PlayerL ] -> [ right PlayerR ]
[ > Player | SlotOpen ] -> cancel
[ > Player | Die | Wall] -> [ Player | Die | Wall]
[ > Player | Die ] -> [ > Player | > Die ]  sfx1

(shifting dice)
[ > Die | Die ] -> [ > Die Shift | > Die Shift ] again
[ > Die Shift | Wall ] -> cancel

(sink dice into holes)
[ Die1 Slot1 ] -> [ SlotClosing1 ] again
[ Die2 Slot2 ] -> [ SlotClosing1 ] again
[ Die3 Slot3 ] -> [ SlotClosing1 ] again
[ Die4 Slot4 ] -> [ SlotClosing1 ] again
[ Die5 Slot5 ] -> [ SlotClosing1 ] again
[ Die6 Slot6 ] -> [ SlotClosing1 ] again
[ Die SlotOpen ] -> [ SlotOpen ] sfx0

(increment rolling dice)
[ > Die6 no Shift] -> [ > Die7 ]
[ > Die5 no Shift] -> [ > Die6 ]
[ > Die4 no Shift] -> [ > Die5 ]
[ > Die3 no Shift] -> [ > Die4 ]
[ > Die2 no Shift] -> [ > Die3 ]
[ > Die1 no Shift] -> [ > Die2 ]
[ Die7 ] -> [  Die1 ]

(rolling dice 2/2)
[ up Die no Shift ] -> [ up Die RollUp ] again
[ left Die no Shift ] -> [ left Die RollLeft ] again
[ right Die no Shift ] -> [ right Die RollRight ] again
[ down Die no Shift ] -> [ down Die RollDown ] again
[ Shift ] -> [ ]


==============
WINCONDITIONS
==============

No SlotOpen
No SlotClosing

=======
LEVELS
=======


Message 1/10: Hide
Message Push a die to roll it in a straight line.

#$####$##
#p####e##
#.####.##
#1$$$$3$#
#..2....#
#.#######
$$$$$$$$$

Message 2/10: Reach
Message Roll a die into a hole with the same value to close the hole.

#$$$$$#
#.....#
#e.1.q#
#.....#
#..####
$$$$$$$

Message 3/10: Intersect
Message All holes must be closed to proceed.

##$$$$#
#$..e.#
#..p..#
#...6.#
#d.1..#
#....##
$$$$$$$

Message 4/10: Nudge
Message Push a row of dice to shift them over without rolling.

#$$$$$$#
#......#
#..12.b#
#.p....#
#.....d#
$$$$$$$$

Message 5/10: Break

#$$$$$$$$#
#.....e..#
#p.223..c#
#......f.#
$$$$$$$$$$

Message 6/10: Divide

#$$$$$$#
#f....f#
#......#
#..12.q#
#..21..#
#......#
#f....f#
$$$$$$$$

Message 7/10: Spill

########
#f..2.c#
#.p21..#
#..12..#
#c.2..f#
########

Message 8/10: Help

#$$$$$$$$$#
#....p....#
#..5...e..#
##.......##
##...5...##
#$.......$#
#..5...e..#
#.........#
$$$$$$$$$$$


Message 9/10: Insert

#$$$$$$$#
#......f#
#.$1$1$.#
#.....ad#
#.$1$1$.#
#p.....f#
$$$$$$$$$

(
Message 9/10: Mix

###$$$###
#$$..c$$#
#...1abb#
#.....$##
#$..1q.##
#.1..4.##
#......##
$$$$$$$$$
)

Message 10/10: Rotate

#$$$$$$$$$$#
#..........#
#..abcdef..#
#..654321q.#
#..........#
$$$$$$$$$$$$

Message Thanks for playing!
